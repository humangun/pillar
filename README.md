# README #

PHP8 url driven, controller model view one file micro framework (13KB). This framework houses MVC architecture only. There are no utilties.

### What does this repository solve? ###

* Resolves back-end structures to satisfy [MVC design pattern](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller).

### What is this repository for? ###

* Start up foundation for building small to large scale transparent MVC applications without the use of any external libraries.
* PHP developers tutorial usage.

### Features ###

* Url driven architecture. The controller path resembles the url path.
* Provides flat url parameters treatment.
* One request per controller architecture.
* Automatic interpolation of the url segments. The url path of /hello-world is translated into HelloWorld.
* Supports modules. House multiple mvc structures within a structure.
* Can forward controller from another controller. (Recursive route dispatching architecture).
* Supports runtime internal redirects (detours). Execute a controller path that is different to what url suggests.
* Just drop-in play.

#### Focus ####

* Continiously increase power to weight ratio.
* Performance and automation in regards to decreasing manual input to achieve a result.
* Maximize on transparency and simplicity, ease of understanding and navigating through structures.
* To retain high capacity for future modernization.

#### Dependencies ####

* Apache 2.0 and higher (https://httpd.apache.org/)
* PHP 8.0 and higher (http://php.net/)

### Multiple Applications Directory Structure ###
```
root
│
└───AppOne
│   │   Assets  (optional)
│   │   Controller
│   │   Model   (optional)
│   │   Modules (optional)
│   │   Utility (optional)
│   │   View    (optional)
│   │   Bootstrap 
|   
└───AppTwo
│   │   Assets  (optional)
│   │   Controller
│   │   Model   (optional)
│   │   Modules (optional)
│   │   Utility (optional)
│   │   View    (optional)
│   │   Bootstrap 
|   
└───Pillar
│   │   Bootstrap
│     
```

### Operational Cycle ###
* Bootstrap - Initiates application level configuration. Creates http request.
* Model - Validates http request.
* View - Sets response parameters.
* Controller - Configures view type output.

### Related Documentation ###

* [Curl Shell - execute endpoints via cli](https://techieground.com/how-to/curl-command-line-parameters/)

### Design Patterns Used ###
* Front Controller - Single entrance point for application.
* Factory - Creates desired controller objects on demand.
* Strategy - Optional, implements object schema or interface to establish operational consistency within model objects.
* MVC - self explanatory.