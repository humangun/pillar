<?php
require_once('src/autoload.php');
use Pillar\Test\src\Test;

$tests = [ 
    'pillar\test\cases\Functional\TestBootstrap',
    'pillar\test\cases\Regression\TestBootstrap'
];

array_walk($tests, function($test) {
    new $test();
});


echo Test::getOutput();
//echo Test::getCases();