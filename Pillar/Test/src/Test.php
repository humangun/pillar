<?php
namespace Pillar\Test\src;

/*
* Framework Test Suite.
* @package Pillar
* @category Core
* @author Denis Nerezov
* @license GNU 
* @version 1.0.0
* @since 1.0.0
*/
abstract class Test
{
    /**
    * Collects all test cases.
    *
    * @var array $cases
    */
    private static array $cases = array();

    /**
    * Collects all test messages.
    *
    * @var array $caseMessagesGrouped
    */
    private static array $caseMessagesGrouped = array();

    /**
    * Collects messages per case.
    *
    * @var array $caseMessages
    */
    protected array $caseMessages = array();

    /**
    * Total number of tests executed.
    *
    * @var int $caseCount
    */
    protected int $caseCount = 0;

    /**
    * Total number of tests executed where the result is fail.
    *
    * @var int $caseCountFailed
    */
    protected int $caseCountFailed = 0;

    /**
    * Set assert options.
    * Automatically execute all tests.
    * Apply response template.
    *
    * @param          object              $testClass      Test class instance.         
    * @return         void
    */
    public function __construct(object $testClass)
    {
        assert_options(ASSERT_ACTIVE, 1);
        assert_options(ASSERT_WARNING, 0);
        assert_options(ASSERT_EXCEPTION, 0);
        assert_options(ASSERT_CALLBACK, function ($file, $line, $code, $info) {
            $this->caseCountFailed ++;
            $msg  = str_replace(['__FUNCTION__', '__NAMESPACE__', '__FILE__', '__LINE__'], explode(',', $info), "\nTest: __FUNCTION__ \nFile: __FILE__:__LINE__");
            
            array_push(self::$caseMessagesGrouped, $msg);
            array_push($this->caseMessages, $msg);
        });

        $class = new \ReflectionClass($testClass);
        $methods = $class->getMethods(\ReflectionMethod::IS_PUBLIC);

        foreach($methods as $method) {
            $methodName = $method->name;
            if(str_contains($methodName, 'test')) {
                $testClass->$methodName();
                
                array_push(self::$cases, $class->getNamespaceName() . '.' . $class->getShortName() . '.' . $methodName);

                $this->caseCount ++;
            }
        }
    }

    /**
    * Get all executed test cases.
    *        
    * @return         string
    */
    public static function getCases() : string
    {
        return implode("\n", self::$cases);
    }

    /**
    * Get total number of tests executed.
    *        
    * @return         string
    */
    public static function getCasesCount() : int
    {
        return count(self::$cases);
    }

    /**
    * Get all failed tests.
    *        
    * @return         string
    */
    public static function getCasesFailed() : string
    {
        return implode("\n", self::$caseMessagesGrouped);
    }

    /**
    * Get total number of failed tests.
    *        
    * @return         string
    */
    public static function getCasesFailedCount() : int
    {
        return count(self::$caseMessagesGrouped);
    }

    /**
    * Get output text.
    *        
    * @return         string
    */
    public static function getOutput() : string
    {
        return self::getCasesCount() . " Tests Executed, " . self::getCasesFailedCount() . " Failed \n" . self::getCasesFailed();
    }
    
    /**
    * Build and return individual case messages.
    *        
    * @return         string
    */
    protected function getCaseOutput() : string
    {
        return "\nTests run:" . $this->caseCount . ", Failed:" . $this->caseCountFailed . implode(' ', $this->caseMessages) . "\n";
    }

    /**
    * Compare single values and their type.
    *
    * @param          mixed               $produced  
    * @param          mixed               $compare
    * @param          string              $method
    * @param          string              $namespace
    * @param          string              $file
    * @param          string              $line      
    * @return         void           
    */
    protected function assertEquals(mixed $produced, mixed $compare, string $method, string $namespace, string $file, string $line) : void
    {
        assert($produced === $compare, implode(',', [$method, $namespace, $file, $line]));
    }

    /**
    * Compare arrays of any depth.
    *
    * @param          array               $produced  
    * @param          array               $compare
    * @param          string              $method
    * @param          string              $namespace
    * @param          string              $file
    * @param          string              $line      
    * @return         void           
    */
    protected function assertEqualsArray(array $produced, array $compare, string $method, string $namespace, string $file, string $line) : void
    {
        assert(empty($this->arrayDiffRecursive($produced, $compare)), implode(',', [$method, $namespace, $file, $line]));
    }

    /**
    * Compare json strings of any complexity.
    *
    * @param          string              $produced  
    * @param          string              $compare
    * @param          string              $method
    * @param          string              $namespace
    * @param          string              $file
    * @param          string              $line      
    * @return         void           
    */
    protected function assertEqualsJson(string $produced, string $compare, string $method, string $namespace, string $file, string $line) : void
    {
        assert(empty($this->arrayDiffRecursive(json_decode($produced, true), json_decode($compare, true))), implode(',', [$method, $namespace, $file, $line]));
    }

    /**
    * Calculates the difference between two arrays of any depth.
    *
    * @param          array               $produced  
    * @param          array               $compare     
    * @return         void           
    */
    private function arrayDiffRecursive(array $produced, array $compare) : array
    {
        $output = array();

        foreach ($produced as $key => $value) {
            if (array_key_exists($key, $compare)) {
                if (is_array($value)) {
                    $slice = $this->arrayDiffRecursive($value, $compare[$key]);

                    if (count($slice)) { 
                        $output[$key] = $slice; 
                    }
                } else {
                    if ($value != $compare[$key]) {
                        $output[$key] = $value;
                    } 
                }
            } else {
                $output[$key] = $value;
            }
        }

        return $output;
    }
}