<?php
spl_autoload_register(function($className) {
    if (file_exists($classPath = str_replace('\\', '/', $className) . '.php')) {
        require_once($classPath);
    } elseif(file_exists($classPath = 'src/' . str_replace('\\', '/', $className) . '.php')) {
        require_once($classPath);
    } elseif(file_exists($classPath = str_replace('\\', '/', $className) . '.php')) {
        require_once($classPath);
    } elseif(file_exists($classPath = '../../' . str_replace('\\', '/', $className) . '.php')) {
        require_once($classPath);
    }
});