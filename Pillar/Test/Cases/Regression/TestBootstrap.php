<?php
namespace Pillar\Test\Cases\Regression;

/*
* Framework's bootstrap Test Suite.
* @package Pillar
* @category Test Suite
* @author Denis Nerezov
* @license GNU 
* @version 1.0.0
* @since 1.0.0
*/
use Pillar\Test\src\Test;
use Pillar\Bootstrap as Bootstrap;

final class TestBootstrap extends Test
{
    /**
    * Instanciate Test core.
    *        
    * @return         void
    */
    public function __construct()
    {
        parent::__construct($this);
    }

    /**
    * Set minimum required to return json view. 
    *        
    * @return         void
    */
    public function testIsolateViewJson() : void
    {
        $app = new Bootstrap();
        $app->setBasePath('pillar/AppOne/')
            ->setRoute('')
            ->setRouteNotFound('error/notfound')
            ->setFlatUrls([])
            ->setModules(['admin' => 'modules/admin',
                'restful' => 'modules/restful'])
            ->dispatch('restful/helloworld.json');
        
        $this->assertEquals(
            (string) $app,
            '{"heading":"Hello World!"}',
            __FUNCTION__, 
            __NAMESPACE__,
            __FILE__, 
            __LINE__
        );
    }

    /**
    * Automatically output cases information. 
    *        
    * @return         string
    */
    public function __toString()
    {
        return $this->getCaseOutput();
    }
}