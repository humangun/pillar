<?php
namespace Pillar\Test\Cases\Functional;

/*
* Framework's bootstrap Test Suite.
* @package Pillar
* @category Test Suite
* @author Denis Nerezov
* @license GNU 
* @version 1.0.0
* @since 1.0.0
*/
use Pillar\Test\src\Test;
use Pillar\Bootstrap as Bootstrap;

final class TestBootstrap extends Test
{
    /**
    * Instanciate Test core.
    *        
    * @return         void
    */
    public function __construct()
    {
        parent::__construct($this);
    }

    /**
    * Set minimum required to return base path.
    * Tests if base route gets set.
    *        
    * @return         void
    */
    public function testSetBasePath() : void
    {
        $app = new Bootstrap();
        $app->setBasePath('pillar/AppOne/');
        
        $this->assertEquals(
            $app->getBasePath(),
            'pillar/AppOne/',
            __FUNCTION__, 
            __NAMESPACE__,
            __FILE__, 
            __LINE__
        );
    }

    /**
    * Set minimum required to return route.
    * Tests if default route gets set.
    *        
    * @return         void
    */
    public function testSetDefaultRoute() : void
    {
        $app = new Bootstrap();
        $app->setRoute('helloworld');
        
        $this->assertEquals(
            $app->getRoute(),
            'helloworld',
            __FUNCTION__, 
            __NAMESPACE__,
            __FILE__, 
            __LINE__
        );
    }

    /**
    * Set minimum required to return route.
    * Tests if default route can behave as flat url with parameters.
    *        
    * @return         void
    */
    public function testDefaultRouteAsFlatUrl() : void
    {
        $app = new Bootstrap();
        $app->setBasePath('pillar/AppOne/');
        $app->setRoute('example/flaturl');
        $app->setFlatUrls(['example/flaturl'  => 'greeting/name']);

        $route = $app->createRoute();
        
        $this->assertEquals(
            $route,
            'example/flaturl/',
            __FUNCTION__, 
            __NAMESPACE__,
            __FILE__, 
            __LINE__
        );
    }

    /**
    * Set minimum required to return route.
    * Tests if default route can behave as module.
    *        
    * @return         void
    */
    public function testDefaultRouteAsModule() : void
    {
        $app = new Bootstrap();
        $app->setBasePath('pillar/AppOne/')
            ->setRoute('admin/helloworld')
            ->setFlatUrls([])
            ->setModules(['admin' => 'modules/admin']);
        
        $this->assertEquals(
            $app->getRoute(),
            'admin/helloworld',
            __FUNCTION__, 
            __NAMESPACE__,
            __FILE__, 
            __LINE__
        );
    }

    /**
    * Set minimum required to return route.
    * Tests if default route can retrieve flat url parameters.
    *        
    * @return         void
    */
    public function testDefaultRouteFlatUrlParameters() : void
    {
        $app = new Bootstrap();
        $app->setBasePath('pillar/AppOne/')
            ->setRoute('example/flaturl/hello/world')
            ->setFlatUrls(['example/flaturl' => 'greeting/name'])
            ->setModules([])
            ->dispatch($app->getRoute());
        
        $this->assertEquals(
            $app->getParameters(),
            ['greeting' => 'hello', 'name' => 'world'],
            __FUNCTION__, 
            __NAMESPACE__,
            __FILE__, 
            __LINE__
        );
    }

    /**
    * Set minimum required to return route.
    * Tests if default route can retrieve flat url parameters.
    *        
    * @return         void
    */
    public function testFlatUrlParameters() : void
    {
        $app = new Bootstrap();
        $app->setBasePath('pillar/AppOne/')
            ->setRoute('helloworld')
            ->setFlatUrls(['example/flaturl' => 'greeting/name'])
            ->setModules([])
            ->dispatch('example/flaturl/hello/world');
        
        $this->assertEquals(
            $app->getParameters(),
            ['greeting' => 'hello', 'name' => 'world'],
            __FUNCTION__, 
            __NAMESPACE__,
            __FILE__, 
            __LINE__
        );
    }

    /**
    * Set minimum required to return route.
    * Tests if default route can retrieve flat url parameters.
    *        
    * @return         void
    */
    public function testModuleFlatUrlParameters() : void
    {
        $app = new Bootstrap();
        $app->setBasePath('pillar/AppOne/')
            ->setRoute('helloworld')
            ->setFlatUrls(['admin/helloworld' => 'greeting/name'])
            ->setModules(['modules/admin'])
            ->dispatch('admin/helloworld/hello/world');
        
        $this->assertEquals(
            $app->getParameters(),
            ['greeting' => 'hello', 'name' => 'world'],
            __FUNCTION__, 
            __NAMESPACE__,
            __FILE__, 
            __LINE__
        );
    }

    /**
    * Automatically output cases information. 
    *        
    * @return         string
    */
    public function __toString()
    {
        return $this->getCaseOutput();
    }
}