# README #

Single file & class MVC framework.

### What does this repository solve? ###

* Resolves framework's object structure or mechanics.

### Public Interface ###
```
Pillar\Bootstrap
│   setBasePath  
│   setRoute            (optional) String
|   createRoute         (optional) String
|   setRouteNotFound    (optional) String
│   setFlatUrls         (optional) Array
│   setModules          (optional) Array
│   dispatch
|
```

### Instantiation ###
```
#!php
$app = new Pillar\Bootstrap();
echo $app->setBasePath('pillar/AppOne/')
    ->setRoute('helloworld')
    ->setRouteNotFound('error/notfound')
    ->setFlatUrls(['example/flaturl'  => 'greeting/name',
        'admin/helloworld' => 'greeting/name',
        'shell/helloworld' => 'greeting/name'])
    ->setModules(['admin'   => 'modules/admin',
        'restful' => 'modules/restful',
        'image'   => 'modules/image',
        'pdf'     => 'modules/pdf',
        'shell'   => 'modules/shell'])
    ->dispatch();
```

Dynamic route creation, when no route is set.
```
#!php
    ->dispatch($app->createRoute());
```