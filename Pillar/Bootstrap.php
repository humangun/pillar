<?php
/**
* Directory path where this file/class resides.
*/
namespace Pillar;

/**
* PHP8 url driven, controller model view one file micro framework (12KB). This framework houses MVC architecture only. There are no utilties.
* @package Pillar
* @category Core
* @author Denis Nerezov
* @license GNU
* @version 1.0.0
* @since 1.0.0
* @link https://bitbucket.org/humangun/pillar/src/master/
*/
class Bootstrap
{
    /**
    * Application relative url base path.
    *
    * @var string $basePath
    */
    private string $basePath;

    /**
    * Application output.
    *
    * @var string $output
    */
    private string $output;

    /**
    * Controller parsed parameters.
    *
    * @var array $output
    */
    private array $parameters = array();
    
    /**
    * Instanciate bootstrap.
    * @param          string         $appDirRoot
    * @return         void                                
    */
    public function __construct() {}

    /**
    * Set current executable path (route).
    *
    * @param          string              $basePath               
    * @return         object              $this                
    */
    public function setBasePath(string $basePath) : Bootstrap
    {
        $this->basePath = $basePath;

        return $this;
    }

    /**
    * Generate route from url path when no route parsed.
    * Modify route if set default route is found within flat urls. 
    *               
    * @return         string              $route
    */
    public function createRoute() : string
    {
        $route = null;

        if (array_key_exists('PATH_INFO', $_SERVER)) {
            $route = trim($_SERVER['PATH_INFO'], '/');
        }

        $route = str_replace($this->getBasePath(), "", $route);
        
        $flatPath = $this->findFlatUrl($this->flatUrls, $this->route);
        if($flatPath) {
            $route = $flatPath . '/' . $route;
        }

        return $route;
    }

    /**
    * Set current executable path (route).
    *
    * @param          string              $route               
    * @return         object              $this                
    */
    public function setRoute(string $route) : Bootstrap
    {
        $this->route = $route;

        return $this;
    }

    /**
    * Set alternative path when no controller found.
    *
    * @param          string              $routeNotFound               
    * @return         object              $this                 
    */
    public function setRouteNotFound(string $routeNotFound) : Bootstrap
    {
        $this->routeNotFound = $routeNotFound;

        return $this;
    }

    /**
    * Set flat url endpoints with associative parameters.
    *
    * @param          array               $flatUrls               
    * @return         object              $this                 
    */
    public function setFlatUrls(array $flatUrls) : Bootstrap
    {
        $this->flatUrls = $flatUrls;

        return $this;
    }

    /**
    * Set multiple module directory residency.
    *
    * @param          array               $modules               
    * @return         object              $this                 
    */
    public function setModules(array $modules) : Bootstrap
    {
        $this->modules = $modules;

        return $this;
    }

    /**
    * Set output with controller's rendered view.
    *
    * @param          string              $output               
    * @return         object              $this                 
    */
    public function setOutput(string $output) : Bootstrap
    {
        $this->output = $output;

        return $this;
    }

    /**
    * Set parsed route.
    * Handle 404 route not found.
    * Build and output application.
    *
    * @param          string        $route
    * @param          array         $parameters
    * @return         object                                    Controller's view output.                   
    */
    public function dispatch(string $route = null, array $parameters = array()) : object
    {
        if($route) {
            $route = $this->prepareRoute($route);
            $this->setRoute($route);
        }

        $controller = $this->createNamespace();

        if(!file_exists($this->createFilepath()) && !class_exists($controller)) {
            if(isset($this->routeNotFound)) {
                $this->setRoute($this->routeNotFound);
            } else {
                exit;
            }
        }

        $this->setOutput((string) $this->controller($controller, $parameters));

        return $this;
    }

    /**
    * Automatically render controller's view object.
    * 
    * @return         string                                                        
    */
    public function __toString()
    {
        return $this->getOutput();
    }

    /**
    * Remove url extension from route.
    * Interpret flat url paths.
    * Apply route interpolation.
    *
    * @param          string        $route
    * @return         string        $route                     
    */
    private function prepareRoute(string $route) : string
    {
        if(array_key_exists('extension', pathinfo($route))) {
            $route = substr($route, 0, strpos($route, "."));
        }

        $flatPath = $this->findFlatUrl($this->flatUrls, $route);
        if($flatPath) {
            $this->setFlatUrlParameters($route, $flatPath);
            $route = $flatPath;
        }

        $route = $this->interpolate($route);

        return $route;
    }

    /**
    * Identify and extract flat url path that matches runtime or current url structure.
    *
    * @param          array         $flatUrls                   Associative collection where keys represent executable paths.
    * @param          string        $route                      Runtime or current executable path.
    * @return         string        $path                       
    */
    private function findFlatUrl(array $flatUrls, string $route) : ? string
    {
        $path = null;

        foreach(array_keys($flatUrls) as $flatUrl) {
            if(str_contains($this->interpolate($route), $flatUrl)) {
                $path = $flatUrl;
                break;
            }
        }

        return $path;
    }

    /**
    * Identify and extract module path that matches current url structure.
    *
    * @param          array         $modules
    * @return         string        $path                     
    */
    private function findModule(array $modules) : ? string
    {
        $path = array_filter($modules, function ($path) {
            $pathSegments = explode('/', $path);
            $name = end($pathSegments);
            if(str_contains($this->interpolate($this->getRoute()), $name)) {
                return $name;
            }
        });

        $path = reset($path);

        return $path;
    }

    /**
    * Apply url interpolation.
    *
    * @param          string          $route                    Runtime or current executable path.
    * @return         string          $segments                 Interpolated executable path.
    */
    private function interpolate(string $route) : string
    {
        $segments = array_map(function ($segment) {
            return str_replace(['-', '_', '%20'], '', $segment);
        }, explode('/', $route));

        return implode('/', $segments);
    }

    /**
    * Extract and set flat url parameters.
    * 
    * @param          string          $route                    Runtime or current executable path.
    * @param          string          $flatUrlPath              Found flat url that relates to runtime executale path.     
    *  
    * @return         void          
    */
    private function setFlatUrlParameters(string $route, string $flatUrlPath) : void
    {
        $values = array_slice(explode('/', $route), count(explode('/', $flatUrlPath)));
        $names  = explode('/', $this->flatUrls[$flatUrlPath]);

        array_walk($names, function($name, $key, $values) {
            if(array_key_exists($key, $values)) {
                $this->parameters[$name] = $values[$key];
            }
        }, $values);
    }

    /**
    * Build executable paths.
    * 
    * @param          string        $name                      Directory where desired class will reside.  
    * @return         string        $path                    
    */
    private function createPath(string $name) : string
    {
        $baseSegments = explode('/', rtrim($this->getBasePath(), '/'));
        $path = array(end($baseSegments));

        array_push($path, $name, $this->getRoute());

        $module = $this->findModule($this->modules);
        if($module) {
            $path   = array();
            $segments = explode('/', $this->getRoute());
            array_shift($segments);
            array_push($path, end($baseSegments), $module, $name);
            $path = array_merge($path, $segments);
        }

        return implode('/', $path);
    }

    /**
    * Build relative file paths.
    * 
    * @param          string        $name                       Directory where desired class will reside.    
    * @param          string        $extension                  File extension to be appended on return.
    * @param          string        $pathDepth                  
    * @return         string        $path                    
    */
    protected function createFilepath(string $name = 'controller', string $extension = 'php', string $pathDepth = "../") : string
    {
        return $pathDepth . $this->createPath($name) . '.' . $extension;
    }

    /**
    * Build class namespace.
    *
    * @param          string        $name                       Directory where desired class will reside.  
    * @return         string        $path                      
    */
    protected function createNamespace(string $name = 'controller') : string
    {
        return str_replace('/', '\\', $this->createPath($name));
    }

    /**
    * Factory pattern where a controller is created.
    *
    * @param          string       $controllerNamespace        Executabler path to controller class.
    * @param          array        $parameters                 View parameters set via controller.
    * @return         object                                   Controller's view output.
    */
    private function controller(string $controllerNamespace, array $parameters = array()) : object
    {
        return new $controllerNamespace($this, ...!empty($parameters) ? $parameters : $this->getParameters());
    }

    /**
    * Set http request headers.
    *
    * @param          array        $headers
    * @return         void                                  
    */
    protected function headers(array $headers) : void
    {
        array_walk($headers, function($info) {
            header($info);
        });
    }

    /**
    * Load view file.
    * Send controller's view parameters to view file.
    * Set header information.
    *
    * @return string
    */
    protected function view(string $filepath, array $parameters, array $headers = array()) : string
    {
        $this->headers($headers);

        extract($parameters, EXTR_REFS);

        ob_start();

        try {
            if (!@include_once($filepath)) {
                throw new \Exception ($filepath . ' view file could not be found.');
            }
        } catch (\Exception $e) {
            echo 'Error: ',  $e->getMessage(), "\n";
        }
        
        $output = ob_get_contents();

        ob_end_clean();

        return $output;
    }

    /**
    * Get application relative base path.
    *
    * @return       string        $this->basePath   
    */
    public function getBasePath() : string
    {
        return $this->basePath;
    }

    /**
    * Get route runtime executable path.
    *
    * @return   string    $this->route 
    */
    public function getRoute() : string
    {
        return $this->route;
    }

    /**
    * Get collection of flat flat url parameters.
    *
    * @return   array    $this->parameters 
    */
    public function getParameters() : array
    {
        return $this->parameters;
    }

    /**
    * Get controller's rendered view.
    *
    * @return   string    $this->output 
    */
    public function getOutput() : string
    {
        return $this->output;
    }
}