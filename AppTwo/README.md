## Application Architecture ##
With no virtual host defined.

One http request per one controller.

###  Endpoints ###
```
http://localhost/pillar/appone Or
http://localhost/pillar/appone/hello-world
```

### Structure ###
```
htdocs
└───pillar
│   └───AppTwo
|   |   └───Controller
|   |   │   └───HelloWorld
|   |   │   │   |   
|   |   └───Model (optional)
|   |   │   │   |   
|   |   └───View
|   |   │   └───HelloWorld
|   |   │   │   |   
|   |   |   index
|   |   |   Bootstrap 
|   |   │ 
```

## Execute Application (index.php) ##

### Serve Default Endpoint ###
Bootstrap.dispatch()
```
#!php
$app = new AppTwo\Bootstrap();
$app->setBasePath('pillar/AppTwo/');
echo $app->dispatch();
```

### Serve Single Endpoint ###
Bootstrap.dispatch()
```
#!php
$app = new AppTwo\Bootstrap();
$app->setBasePath('pillar/AppTwo/');
echo $app->dispatch('helloworld');
```

### Serve Multiple Endpoints At Once Synchronously ###
Bootstrap.dispatch()
```
#!php
$app = new AppTwo\Bootstrap();
$app->setBasePath('pillar/AppTwo/');
echo $app->dispatch('helloworld');
```

### Alternative Route Definition ###
Bootstrap.setRoute()
```
#!php
$app = new AppTwo\Bootstrap();
$app->setBasePath('pillar/AppTwo/');
$app->setRoute('helloworld');
echo $app->dispatch();
```

### Dynamic Route Generation ###
Bootstrap.createRoute()
```
#!php
$app = new AppTwo\Bootstrap();
$app->setBasePath('pillar/AppTwo/');
$route = $app->createRoute();
echo $app->dispatch($route);
```

### Chaining Bootstrap Methods ###
```
#!php
$app = new AppTwo\Bootstrap();
$app->setBasePath('pillar/AppTwo/')
    ->dispatch($app->createRoute());

echo $app;
```