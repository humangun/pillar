<?php
require_once('autoload.php');

$app = new AppTwo\Bootstrap();
$app->setBasePath('pillar/AppTwo/')
    ->dispatch($app->createRoute());
echo $app;