<!DOCTYPE html>
<html lang="en">
<head>
  <title>
    Page not found - PhpMvcLite
  </title>
  <meta http-equiv="content-type" content="text/html"/>
  <meta http-equiv="content-language" content="en"/>
  <meta charset="utf-8" />
</head>

<body>
  <main>
    <header>
        <h1>Page Not Found</h1>
    </header>
  </main>
  <footer>
    <small>Pillar 2022</small>
  </footer>
</body>
</html>
