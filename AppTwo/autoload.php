<?php
spl_autoload_register(function($className) {
    $rootPath  = dirname(dirname(__FILE__));
    $classPath = $rootPath . '/' . str_replace('\\', '/', $className) . '.php';

    if (file_exists($classPath)) {
        require_once($classPath);
    }
});