<?php
// Controller's directory path.
namespace AppTwo\Controller;

// Objects required by this controller.
use AppTwo\Bootstrap;

/**
* @category  Application
* @package   Mvc Lite
* @author    Denis Nerezov <dnerezov@gmail.com>
* @copyright (c) 2021 Denis Nerezov
* @link      https://github.com/dnerezov/php-mvclite-workspace/wiki
*/
final class HelloWorld extends Bootstrap
{
    /**
    * Set default view parameter heading.
    *
    * @var string $heading 
    */
    public string $heading = 'Hello...';

    /**
    * Instanciate controller.
    * Set view parameters.
    * Flat url parameters are parsed as construct arguments on demand and not required.
    *
    * @param        string           Multiple dynamic flat url parameters.
    *
    * @return       void
    */
    public function __construct(object $app, $planet = 'World!')
    {
        $this->heading = 'Hello ' . $planet;
    }

    /**
    * Auto output controller's view content as string.
    * Set view file.
    * Push controller properties to view / set view parameters.
    * Set http response headers.
    *
    * @return       string
    */
    function __toString() 
    {
        //pillar/AppTwo/
        //echo $this->createFilepath('view');
        //C:\Apache24\htdocs\pillar\AppTwo\View\HelloWorld.php
        return $this->view('../AppTwo/View/HelloWorld.php', (array) $this, ["HTTP/1.1 200 OK", "Content-Type: text/html; charset=utf-8"]);
    }
}
