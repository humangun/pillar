<?php
// Controller's directory path.
namespace AppTwo\Controller\Error;

// Objects required by this controller.
use AppTwo\Bootstrap;

/**
* @category  Application
* @package   Mvc Lite
* @author    Denis Nerezov <dnerezov@gmail.com>
* @copyright (c) 2021 Denis Nerezov
* @link      https://github.com/dnerezov/php-mvclite-workspace/wiki
*/
final class Notfound extends Bootstrap
{   
    /**
    * Set default view filepath.
    *
    * @var string $viewFile 
    */
    public string $viewFile = 'view/error/default.php';
    
    /**
    * Instanciate controller.
    * Set view parameters.
    *
    * @param        object           $bootstrap         Modified (runtime) bootstrap instance.
    * @param        array            $parameters        Parsed parameters when forwarded (invoked) from another controller. 
    *
    * @return       void
    */
    public function __construct(object $app)
    {
        $this->viewFile = $app->buildPath('view');
    }

    /**
    * Auto output controller's view content as string.
    * Set view file.
    * Push controller properties to view / set view parameters.
    * Set http response headers.
    *
    * @return       string
    */
    function __toString() 
    {
        return $this->view($this->viewFile, (array) $this, ["HTTP/1.0 404 Not Found", "Content-Type: text/html; charset=utf-8"]);
    }
}
