<!DOCTYPE html>
<head>
  <title>
    Hello World - Pillar
  </title>
  <meta http-equiv="content-type" content="text/html"/>
  <meta http-equiv="content-language" content="en"/>
  <html lang="en">
</head>

<body>
  <header>
    <h1>
      <?php echo $heading ?>
    </h1>
  </header>
  <main>
    <ul>
      <li>
        <a href="./">Default Landing Page</a>
      </li>
      <li>
        <a href="example/flat-url/Hello/World">Flat Url Parameters</a>
      </li>
      <li>
        <a href="admin/helloworld/Welcome/Admin">Module</a>
      </li>
      <li>
        <a href="example/dispatch">Forward Controller</a>
      </li>
      <li>
        <a href="restful/helloworld.json">Json View</a>
      </li>
      <li>
        <a href="image/helloworld.jpg">Image View</a>
      </li>
      <li>
        <a href="pdf/helloworld.download">Pdf Download</a>
      </li>
    </ul>
  </main>
  <footer>
    <small>Pillar 2022</small>
  </footer>
</body>
</html>