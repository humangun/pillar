<!DOCTYPE html>
<head>
  <title>
    Flat Url Example - Pillar
  </title>
  <meta http-equiv="content-type" content="text/html"/>
  <meta http-equiv="content-language" content="en"/>
  <html lang="en">
</head>

<body>
  <header>
    <h1>Flat Url Parameters</h1>
  </header>
  <main>
    <p><?php echo $greeting ?> <?php echo $name ?></p>
    <p>Change or remove any of the last two segments in url to see the response.</p>
  </main>
  <footer>
    <small>Pillar 2022</small>
  </footer>
</body>
</html>