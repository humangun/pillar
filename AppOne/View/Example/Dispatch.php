<!DOCTYPE html>
<head>
  <title>
    Forward Controller - Pillar
  </title>
  <meta http-equiv="content-type" content="text/html"/>
  <meta http-equiv="content-language" content="en"/>
  <html lang="en">
</head>

<body>
  <header>
      <h1>Forward Controller / Internal Dispatches</h1>
  </header>
  <main>
    <p>Execute another controller paths within a controller.</p>
    
    <p>Bellow is the output of: <strong>helloworld</strong></p>
    <?php echo $dispatched ?>
    <p>Bellow is the output of: <strong>example/flat-url/Hey/Planet</strong></p>
    <?php echo $dispatchedFlat ?>
  </main>
  <footer>
    <small>Pillar 2022</small>
  </footer>
</body>
</html>