<?php
// Controller's directory path.
namespace AppOne\Modules\Image\Controller;

// Objects required by this controller.
use AppOne\Bootstrap;

/**
* Set default controller property values.
* Extends bootstrap to get initial state, access default application settings and reusable core bootstrap methods.
* @category  Application (Development)
* @package   Mvc Lite
* @author    Denis Nerezov <dnerezov@gmail.com>
* @copyright (c) 2018 Denis Nerezov
* @link      https://github.com/dnerezov/php-mvclite-workspace/wiki
*/
class HelloWorld extends Bootstrap
{
    /**
    * Instanciate controller.
    * Set view parameters.
    *
    * @param        object           $bootstrap         Modified (runtime) bootstrap instance.
    * @param        array            $parameters        Parsed parameters when forwarded (invoked) from another controller. 
    *
    * @return       void
    */
    public function __construct(object $app)
    {
        $this->headers(['Content-type: image/jpeg']);

        readfile('Assets/img/scene.jpg');
    }
}
