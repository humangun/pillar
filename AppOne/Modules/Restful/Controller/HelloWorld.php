<?php
// Controller's directory path.
namespace AppOne\Modules\Restful\Controller;

// Objects required by this controller.
use AppOne\Bootstrap;

/**
* @category  Application
* @package   Mvc Lite
* @author    Denis Nerezov <dnerezov@gmail.com>
* @copyright (c) 2021 Denis Nerezov
* @link      https://github.com/dnerezov/php-mvclite-workspace/wiki
*/
class HelloWorld extends Bootstrap
{
    /**
    * Set default view parameter heading.
    *
    * @var string $heading 
    */
    public string $heading = 'Hello...';

    /**
    * Instanciate controller.
    * Set view parameters.
    *
    * @param        object           $bootstrap         Modified (runtime) bootstrap instance.
    * @param        array            $parameters        Parsed parameters when forwarded (invoked) from another controller. 
    *
    * @return       void
    */
    public function __construct()
    {
        //$this->headers(["HTTP/1.1 200 OK", "Content-Type: application/json"]);
        $this->heading = 'Hello World!';
    }

    /**
    * Output controller's view content as string.
    *
    * @return       string
    */
    function __toString() 
    {
        return json_encode($this);
    }
}
