<?php
// Controller's directory path.
namespace AppOne\Modules\Pdf\Controller;

// Objects required by this controller.
use AppOne\Bootstrap;

/**
* Set default controller property values.
* Extends bootstrap to get initial state, access default application settings and reusable core bootstrap methods.
* @category  Application (Development)
* @package   Mvc Lite
* @author    Denis Nerezov <dnerezov@gmail.com>
* @copyright (c) 2018 Denis Nerezov
* @link      https://github.com/dnerezov/php-mvclite-workspace/wiki
*/
class HelloWorld extends Bootstrap
{
    /**
    * Instanciate controller.
    * Set view parameters.
    * Flat url parameters are parsed as construct arguments on demand and not required.
    *
    * @param        object           Application and core bootstrap instance.
    * @param        string           Multiple dynamic flat url parameters. Remove { = '' } to make flat url parameter required
    *
    * @return       void
    */
    public function __construct(object $app)
    {
        $fileName = 'project-proposal.pdf';
        $document = 'Assets/pdf/' . $fileName;

        $this->headers(["Content-Type: application/pdf", "Content-disposition: attachment; filename=" . $fileName]);
        readfile($document);
    }
}
