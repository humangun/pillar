<?php
/**
* Controller's directory path.
*/
namespace AppOne\Modules\shell\Controller;

// Objects required by this controller.
use AppOne\Bootstrap;

/*
* This controller services CLI requests.
* curl http://localhost/pillar/appone/shell/helloworld/Hi/Santa
* curl http://localhost/pillar/appone/shell/helloworld -d greeting=Hi -d name=Santa
*/

/**
* @category  Application
* @package   Mvc Lite
* @author    Denis Nerezov <dnerezov@gmail.com>
* @copyright (c) 2021 Denis Nerezov
* @link      https://github.com/dnerezov/php-mvclite-workspace/wiki
*/
class HelloWorld extends bootstrap
{
    /**
    * @var string $greeting 
    */
    public string $greeting = 'Hello';

    /**
    * @var string $name 
    */
    public string $name = 'World!';

    /**
    * Instanciate controller.
    * Set view parameters.
    *
    * @param        object           $app               Modified (runtime) bootstrap instance.
    * @param        array            *                  Multiple dynamic flat url parameters. Remove { = '' } to make flat url parameter required
    *
    * @return       void
    */
    public function __construct(object $app, $greeting = '', string $name = '')
    {
        if($greeting) {$this->greeting = $greeting;}
        if($name) {$this->name = $name;}

        if(!empty($_REQUEST['greeting'])) {$this->greeting = $_REQUEST['greeting'];}
        if(!empty($_REQUEST['name'])) {$this->name = $_REQUEST['name'];}
    }

    /**
    * Auto output controller's view content as string.
    * Set cli response.
    *
    * @return       string
    */
    function __toString() 
    {
        return $this->greeting . ' ' .  $this->name;
    }
}
