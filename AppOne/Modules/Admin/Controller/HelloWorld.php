<?php
/**
* Controller's directory path.
*/
namespace AppOne\Modules\Admin\Controller;

// Objects required by this controller.
use AppOne\Bootstrap;

/**
* @category  Application
* @package   Mvc Lite
* @author    Denis Nerezov <dnerezov@gmail.com>
* @copyright (c) 2021 Denis Nerezov
* @link      https://github.com/dnerezov/php-mvclite-workspace/wiki
*/
class HelloWorld extends bootstrap
{
    /**
    * Relative path to view file.
    *
    * @var string $viewFilePath 
    */
    public string|null $viewFilePath = 'Modules/Admin/View/HelloWorld.php';

    /**
    * Instanciate controller.
    * Set view parameters.
    *
    * @param        object           $app               Modified (runtime) bootstrap instance.
    * @param        array            *                  Multiple dynamic flat url parameters. Remove { = '' } to make flat url parameter required
    *
    * @return       void
    */
    public function __construct(object $app, $greeting = '', string $name = '')
    {
        $this->greeting = $greeting;
        $this->name = $name;

        $this->viewFilePath = $app->createFilepath('view', 'phtml');
    }

    /**
    * Auto output controller's view content as string.
    * Set view file.
    * Push controller properties to view / set view parameters.
    * Set http response headers.
    *
    * @return       string
    */
    function __toString() 
    {
        return $this->view($this->viewFilePath, (array) $this, ["HTTP/1.1 200 OK", "Content-Type: text/html; charset=utf-8"]);
    }
}
