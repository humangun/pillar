<?php
/*
* Declare application namespace.
* The namespace reflects the application folder name where app files reside.
*/
namespace AppOne;

/*
* List all the library class usages that are required to run an application.
*/
use Pillar\Bootstrap as Core;

/*
* Application dedicated bootstrap file. Not shared by other apps. Declares application options/settings.
* @package MvcLite
* @category Core
* @author Denis Nerezov
* @license GNU 
* @version 1.0.0
* @since 1.0.0
*/
class Bootstrap extends Core
{
    /**
    * Set default executable path/landing page. Executes when current url has only domain name in it without any trailing url paths.
    *
    * @var $route
    */
    protected string $route = 'helloworld';

    /**
    * Set relative executable path to controller to handle "not found" requests.
    *
    * @var $routeNotFound     string
    */
    protected string $routeNotFound = 'error/notfound';

    /**
    * Store executable url paths that are to be treated as flat urls with parameters.
    * Apply flat url parameters to modules.
    *
    * @var $flatUrls      array
    */
    protected array $flatUrls = array(
        'example/flaturl'  => 'greeting/name',
        'admin/helloworld' => 'greeting/name',
        'shell/helloworld' => 'greeting/name'
    );

    /**
    * Break down application into multiple MVC portals by using modules.
    * Point first url segment to execute paths within specified module directories.
    * Example: When set $modules = ['admin' => 'Modules/Admin'], Url: http://localhost/admin/user/create points to execute any controller path within /Modules/Admin directory.
    *
    * @var $modules    array
    */
    protected array $modules = array(
        'modules/admin',
        'modules/restful',
        'modules/image',
        'modules/pdf',
        'modules/shell'
    );

    /**
    * Instanciate all common utilities required.
    *
    * @return void
    */
    public function __construct()
    {

    }

    /**
    * Protected method example.
    *
    * @return       string           $message
    */
    protected function applicationWideMethod() : string
    {
        return "This shared method is accessible from any controller within the application.";
    }
}
