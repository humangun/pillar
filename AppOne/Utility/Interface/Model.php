<?php
namespace AppOne\Utility\Interface;

interface Model
{
    public function validate($jsonSchema = null);
}