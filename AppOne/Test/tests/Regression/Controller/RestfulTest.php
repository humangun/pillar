<?php

use PHPUnit\Framework\TestCase;

final class RestfulTest extends TestCase
{
    public function testMyWorldXX()
    {
        $app = new AppOne\Bootstrap();
        $app->setBasePath('pillar/AppOne/');
        
        $this->assertEquals(
            "Hello World!",
            $app->dispatch('shell/helloworld')
        );
    }
}