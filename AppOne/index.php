<?php
ini_set('error_log', 'Errors.log');

//Lazy load application files.
require_once('autoload.php');

//Initiate application.
$app = new AppOne\Bootstrap();

//Set application root directory.
$app->setBasePath('pillar/AppOne/');

//Set default / landing route.
$app->setRoute('helloworld');

//Automatically generate endpoint upon sending url request. No need to predefine endpoints used.
$route = $app->createRoute();

//Execute application.
$app->dispatch($route);

//Print endpoint response.
echo $app;

/*
Execute and a particular endpoint.

$app->setRoute('example/flaturl');
echo $app->dispatch();

Or

echo $app->dispatch('example/flaturl');
*/


//Another way to execute an application.

/*
$app = new Pillar\Bootstrap();
$app->setBasePath('pillar/AppOne/')
    ->setRoute('example/detour')
    ->setFlatUrls(['example/detour' => 'greeting/name'])
    ->setDetours(['example/detour' => 'example/flaturl'])
    ->setModules([]);

    $route = $app->createRoute();
    echo $route;
    $app->dispatch($route);

echo $app;
*/