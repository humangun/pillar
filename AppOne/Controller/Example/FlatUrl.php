<?php
// Controller's directory path.
namespace AppOne\Controller\Example;

// Objects required by this controller.
use AppOne\Bootstrap;
use AppOne\Model\Example as Model;

/**
* Set default controller property values.
* Extends bootstrap to get initial state, access default application settings and reusable core bootstrap methods.
* @category  Application (Development)
* @package   Mvc Lite
* @author    Denis Nerezov <dnerezov@gmail.com>
* @copyright (c) 2018 Denis Nerezov
* @link      https://github.com/dnerezov/php-mvclite-workspace/wiki
*/
final class FlatUrl extends Bootstrap
{
    /**
    * Set model.
    *
    * @var object $model
    */
    private object $model;

    /**
    * Set default view parameter greeting.
    *
    * @var string $greeting 
    */
    public string $greeting = 'Hello...';

    /**
    * Set default view parameter name.
    *
    * @var string $name 
    */
    public string $name = 'Bot';

    /**
    * Instanciate controller.
    * Set view parameters.
    * Flat url parameters are parsed as construct arguments on demand and not required.
    *
    * @param        object           Application and core bootstrap instance.
    * @param        string           Multiple dynamic flat url parameters. Remove { = '' } to make flat url parameter required
    *
    * @return       void
    */
    public function __construct(object $app, $greeting = '', string $name = '')
    {
        $this->greeting = $greeting;
        $this->name = $name;
    }

    /**
    * Auto output controller's view content as string.
    * Set view file.
    * Push controller properties to view / set view parameters.
    * Set http response headers.
    *
    * @return       string
    */
    function __toString() 
    {
        return $this->view('view/example/flaturl.php', (array) $this, ["HTTP/1.1 200 OK", "Content-Type: text/html; charset=utf-8"]);
    }
}
