<?php
// Controller's directory path.
namespace AppOne\Controller\Example;

// Objects required by this controller.
use AppOne\Bootstrap;

/**
* Set default controller property values.
* Extends bootstrap to get initial state, access default application settings and reusable core bootstrap methods.
* @category  Application (Development)
* @package   Mvc Lite
* @author    Denis Nerezov <dnerezov@gmail.com>
* @copyright (c) 2018 Denis Nerezov
* @link      https://github.com/dnerezov/php-mvclite-workspace/wiki
*/
final class Dispatch extends Bootstrap
{
    /**
    * Set default view parameter dispatched.
    *
    * @var string $dispatched 
    */
    public string | null $dispatched = null;

    /**
    * Instanciate controller.
    * Set view parameters.
    * Flat url parameters are parsed as construct arguments on demand and not required.
    *
    * @param        object           Application and core bootstrap instance.
    * @param        string           Multiple dynamic flat url parameters. Remove { = '' } to make flat url parameter required
    *
    * @return       void
    */
    public function __construct(object $app)
    {
        $this->dispatched = $app->dispatch('helloworld', ['planet' => 'Earth']);
        $this->dispatchedFlat = $app->dispatch('example/flat-url/Hey/Planet');
    }

    /**
    * Auto output controller's view content as string.
    * Set view file.
    * Push controller properties to view / set view parameters.
    * Set http response headers.
    *
    * @return       string
    */
    function __toString() 
    {
        return $this->view('view/example/dispatch.php', (array) $this, ["HTTP/1.1 200 OK", "Content-Type: text/html; charset=utf-8"]);
    }
}
