## Application Architecture ##
With no virtual host defined.

One http request per one controller.

###  Endpoints ###
```
http://localhost/pillar/appone/example/hello-world
```

### Structure ###
```
htdocs
└───pillar
│   └───AppOne
|   |   └───Controller
|   |   │   └───Example
|   |   │   │   |   HelloWorld
|   |   │   │   |   
|   |   └───Model (optional)
|   |   |   └───Example
|   |   │   │   |   
|   |   └───View
|   |   │   └───Example
|   |   |   │   |   HelloWorld
|   |   │   │   |   
|   |   |   index
|   |   |   Bootstrap 
|   |   │ 
```

## Execute Application (index.php) ##

### Serve Default Endpoint ###
Bootstrap.dispatch()
```
#!php
$app = new AppOne\Bootstrap();
$app->setBasePath('pillar/AppOne/');
echo $app->dispatch();
```

```
http://localhost/pillar/appOne executes path example/hello-world if default route defined as such
```

### Serve Single Endpoint ###
Bootstrap.dispatch()
```
#!php
$app = new AppOne\Bootstrap();
$app->setBasePath('pillar/AppOne/');
echo $app->dispatch('example/hello-world');
```

### Serve Multiple Endpoints At Once Synchronously ###
Bootstrap.dispatch()
```
#!php
$app = new AppOne\Bootstrap();
$app->setBasePath('pillar/AppOne/');
echo $app->dispatch('example/hello-world');
echo $app->dispatch('example/form');
```

### Alternative Route Definition ###
Bootstrap.setRoute()
```
#!php
$app = new AppOne\Bootstrap();
$app->setBasePath('pillar/AppOne/');
$app->setRoute('example/form');
echo $app->dispatch();
```

### Dynamic Route Generation ###
Bootstrap.createRoute()
```
#!php
$app = new AppOne\Bootstrap();
$app->setBasePath('pillar/AppOne/');
$route = $app->createRoute();
echo $app->dispatch($route);
```

### Chaining Bootstrap Methods ###
```
#!php
$app = new AppOne\Bootstrap();
$app->setBasePath('pillar/AppOne/')
    ->dispatch($app->createRoute());

echo $app;
```

## Application Options ##

### Set Default Route  ###
AppOne/Bootstrap.$route
```
#!php
$route = 'example/helloworld';
```
Url: 
http://localhost/pillar/appOne/ 
executes path: 
/controller/example/helloworld

### Set Flat Url Parameters ###
AppOne/Bootstrap.$flatUrls
```
#!php
$flatUrls = array(
    'example/flaturl'  => 'greeting/name',
    'admin/helloworld' => 'greeting/name'
);
```

```
http://localhost/pillar/appOne/example/flat-url/hi/Santa executes path example/flaturl
http://localhost/pillar/appOne/example/admin/hi/Santa executes path /Modules/Admin/Controller/example/flaturl
```

### Set Modules ###
AppOne/Bootstrap.$modules
```
#!php
$modules = array(
    'admin'   => 'modules/admin',
    'restful' => 'modules/restful',
    'image'   => 'modules/image',
    'pdf'     => 'modules/pdf',
    'shell'   => 'modules/shell'
);
```

```
http://localhost/pillar/appOne/admin/helloWorld executes path /Modules/Admin/Controller/helloWorld
http://localhost/pillar/appOne/restful/helloWorld executes path /Modules/Restful/Controller/helloWorld
```

### Set 404 or Path Not Found ###
AppOne/Bootstrap.$pathNotFound
```
#!php
$pathNotFound = 'error/notfound';
```

```
http://localhost/pillar/appOne/adm11n/helloWorldABCD executes path /Controller/Error/notfound
```

## Endpoint Interpolation ##
The endpoint can seamlessly interpret url special characters used within the endpoint.
```
http://localhost/pillar/appOne/hello-world.json
http://localhost/pillar/appOne/he-llo_wor-ld.json
http://localhost/pillar/appOne/hello-wo_rld.json
http://localhost/pillar/appOne/hello__world
```
In all cases executes the same filepath /Controller/Example/HelloWorld

## Url Extensions ##
Extensions are dynamic and can be whatever due to the fact that the feedback is determined by the controller. Any url extension can be used to identify whatever resource. Apart from all the known valid extensions, the design permits any alphanumeric string.
```
http://localhost/pillar/appOne/hello-world.apple
http://localhost/pillar/appOne/hello-world.joh1n
http://localhost/pillar/appOne/hello-world.json
http://localhost/pillar/appOne/hello-world.{*}
```

## Shell ##
Execute endpoints via command line interface.
```
#!shell
curl http://localhost/pillar/appone/shell/helloworld/Hi/Santa
curl http://localhost/pillar/appone/shell/helloworld -d greeting=Hi -d name=Santa
```

## Unit Tests ##
Execute all tests under tests/ via command line interface.
```
cd .. pillar/AppOne/Test
php phpunit-9.5.phar --bootstrap src/autoload.php tests
```
```
php phpunit-9.5.phar --bootstrap src/autoload.php tests/Regression/Controller
```
```
php phpunit-9.5.phar --bootstrap src/autoload.php tests/Regression/Controller/RestfulTest.php
```