<?php
/**
* Directory path where model resides.
*/
namespace AppOne\Model;

/**
* Define all required library objects for model to operate.
*/
use AppOne\Utility\Interface\Model;

/**
* Set default model property values.
* Validate request parmeters. Define custom validation rules.
* Accommodate application business logic.
* Transact with resources such as database.
* @category  Application (Development)
* @package   Mvc Lite
* @author    Denis Nerezov <dnerezov@gmail.com>
* @copyright (c) 2018 Denis Nerezov
* @link      https://github.com/dnerezov/php-mvclite-workspace/wiki
*/
class Example implements Model
{
    /**
    * Instanciate model class.
    *
    * @return       void
    */
    public function __construct() {}

    /**
    * Run this function prior the main request process.
    *
    * @return       Form
    */
    public function validate($jsonSchema = null) : Form
    {
        return $this;
    }
}
